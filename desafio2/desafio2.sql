SELECT `departments`.`dept_name` as Nome_Departamento,   
CONCAT(`titles`.`title`," ",`employees`.`first_name`," ",`employees`.`last_name`) as Nome_Completo_Funcionario, 
DATEDIFF(`dept_emp`.`to_date`,`dept_emp`.`from_date`) as Dias_Trabalhados_Departamento
FROM `employees`  
INNER JOIN `dept_emp` ON `employees`.`emp_no` = `dept_emp`.`emp_no` 
INNER JOIN `departments`  ON `departments`.`dept_no` = `dept_emp`.`dept_no` 
INNER JOIN `titles`  ON `titles`.`emp_no` = `employees`.`emp_no` 
ORDER BY Dias_Trabalhados_Departamento DESC LIMIT 10