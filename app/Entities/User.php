<?php
    
    namespace App\Entities;
    use Illuminate\Notifications\Notifiable;
    use Illuminate\Foundation\Auth\User as Athenticatable;
    
    class User extends Athenticatable
    {
        use Notifiable;
    
    
        public $timestamps = true;
        protected $table = 'users';
        protected $fillable = ['name', 'password', 'email'];
        protected $hidden = ['password', 'remember_token'];
    
    }
    