<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\RoomCreateRequest;
use App\Http\Requests\RoomUpdateRequest;
use App\Repositories\RoomRepository;
use App\Validators\RoomValidator;
use App\Services\RoomService;

/**
 * Class RoomsController.
 *
 * @package namespace App\Http\Controllers;
 */
class RoomsController extends Controller
{
  
    protected $repository;

    protected $validator;

    protected $service;

    public function __construct(RoomRepository $repository, RoomValidator $validator, RoomService $service)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->service = $service;
    }

    
    public function index()
    {
        $rooms = $this->repository->all();

        return view ('room.index', [
            'rooms' => $rooms
        ]);
    }

    public function cadastrarSala()
    {
        return view ('room.cadastrar');
    }

    public function store(RoomCreateRequest $request)
    {
        $request = $this->service->store($request->all());
        $room = $request['success'] ? $request['data'] : null;

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages']
        ]);

        return view ('room.cadastrar',[
            'room' => $room
        ]);
    }

    public function show($id)
    {
        $room = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $room,
            ]);
        }

        return view('rooms.show', compact('room'));
    }

    public function edit($id)
    {
        $room = $this->repository->find($id);

        return view('rooms.edit', compact('room'));
    }

    public function update(RoomUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $room = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Room updated.',
                'data'    => $room->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Room deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Room deleted.');
    }
}
