<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ControllerSalas extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function homepage()
    {
        $title = "Homepage do sistema Tobias";
        return view('welcome', [
            'title' => $title
        ]);
    }

    public function cadastrar()
    {
        echo"Tela de cadastro";
    }

    
    public function autenticar()
    {
        echo"Tela de login";
    }



}
