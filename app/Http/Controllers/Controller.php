<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function homepage()
    {
        return view('user.login');
    }
    
    public function autenticar()
    {
        return view('user.login');
    }
    public function cadastrar()
    {
        return view ('room.cadastrar');
    }
    public function logout(Request $request) {
        Auth::logout();
        return $this->homepage();
      }



}
