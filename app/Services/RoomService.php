<?php

namespace App\Services;

use App\Repositories\RoomRepository;
use App\Validators\RoomValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Database\QueryException;
use Exception;  


class RoomService{

    private $repository;
    private $validator;

    public function __construct(RoomRepository $repository, RoomValidator $validator)
    {
       $this->repository = $repository;
       $this->validator = $validator;
    }

    public function store($data){
        try{

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
            $room = $this->repository->create($data);

            return[
                'success' => true,
                'messages' => "Sala cadastrada com sucesso!",
                'data' => $room,
            ];

        } catch(\Exception $e){

            switch(get_class($e)){
                case QueryException::class: return ['success' => false, 'messages' => $e->getMessage()];    
                case ValidatorException::class:  return ['success' => false, 'messages' => $e->getMessageBag()];    
                case Exception::class:  return ['success' => false, 'messages' => $e->getMessage()];    
                default: return ['success' => false, 'messages' => get_class($e)];    
            }
        }
    }

    public function update(){

    }
}