<?php

namespace App\Services;

use App\Repositories\ScheduleRepository;
use App\Validators\ScheduleValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Database\QueryException;
use Exception;  
use App\Services\Auth;


class ScheduleService{

    private $repository;
    private $validator;

    public function __construct(ScheduleRepository $repository, ScheduleValidator $validator)
    {
       $this->repository = $repository;
       $this->validator = $validator;
    }

    public function store($data){
        try{
            
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
            

            $schedule = $this->repository->create($data);
            
            return[
                'success' => true,
                'messages' => "Agendado com sucesso!",
                'data' => $schedule,
            ];

        } catch(\Exception $e){

            switch(get_class($e)){
                case QueryException::class: return ['success' => false, 'messages' => $e->getMessage()];    
                case ValidatorException::class:  return ['success' => false, 'messages' => $e->getMessageBag()];    
                case Exception::class:  return ['success' => false, 'messages' => $e->getMessage()];    
                default: return ['success' => false, 'messages' => get_class($e)];    
            }
        }
    }

    public function update(){

    }
    public function adicionaHora($hora_dois){
        $hora_um = "01:00";
        $h =  strtotime($hora_um);
        $h2 = strtotime($hora_dois);

        $minutos = date("i", $h2);
        $hora = date("H", $h2);

        $temp = strtotime("+$minutos minutes", $h);
        $temp = strtotime("+$hora hours", $temp);
        $nova_hora = date('H:i', $temp);
        return $nova_hora;
    }

    public function delete($data){
        $Reserva = $this->repository->findByField('id_user', $data['id_user']);
        if(sizeof($Reserva)!=0){
            $idReserva = (string)$Reserva[0]['id'];
            $this->repository->delete($idReserva);
        return[
            'success' => true,
            'messages' => "Suas reservas foram excluídas!",
            'data' => $data
        ];
        }else{
            return[
                'success' => true,
                'messages' => "Você não possui reservas!",
                'data' => $data
            ];
        }
        
        
        
    }
    
    public function verifyDate($data){
        $verificaReserva = $this->repository->findByField('id_user', $data['id_user']);
        $dates = $this->repository->findByField('id_room', $data['id_room']);
        $currentDay = date('Y:m:d', strtotime($data['day']));
        $currentHour = date('H:i', strtotime($data['hour']));


        if(sizeof($verificaReserva)!=0){
            return[
                'success' => false,
                'messages' => "A reserva não foi concluída, você já tem uma reserva!",
                'data' => $data
            ];
           }else{

           }
       if(sizeof($dates)==0){
        return $this->store($data);
       }else{

       }
       $equals=false;
       foreach ($dates as $date){
        $hour = date('H:i', strtotime($date['hour']));
        $hourEnd = $this->adicionaHora($date['hour']);
        $hourEnd = date('H:i', strtotime($hourEnd));
        $day = date('Y:m:d', strtotime($date['day']));
           if( $currentDay == $day && $hour == $currentHour){
            $equals=true;
           } else if($currentDay == $day && $currentHour>$hour && $currentHour<$hourEnd){
            return[
                'success' => false,
                'messages' => "Sala já locada neste horário!",
                'data' => $data
            ];
           }
       }
       if($equals==false){
           
        return $this->store($data);
    }else{
     return[
         'success' => false,
         'messages' => "Verifique se esta data e horários estão disponíveis!",
         'data' => $data
     ];
    }
       
       
    }
}