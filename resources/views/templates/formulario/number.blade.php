<label class="{{ $class ?? null }}">

    <span>{{ $label ?? $input ?? "ERRO" }}</span>
    {!! Form::time($input, $value ?? null, $attributes) !!}

</label>