<!DOCTYPE html>
<html lang="pt-br">
    <header>
        <meta charset="UTF-8">
        <title>Meeting rooms</title>
        
        <link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}"
        <link href="https://fonts.googleapis.com/css?family=Fredoka+One" rel="stylesheet">
        @yield('css-view')
    </header>
    <body>
        @include('templates.menu-lateral')
        <section id="view-content">
        @yield('content-view')
    </section>
        @yield('js-view')
    </body>
</html>