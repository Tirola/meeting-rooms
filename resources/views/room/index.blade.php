@extends('templates.master')

@section('content-view')
<table class="default-table">
        <thead>
            <tr>
                <td>ID</td>
                <td>DESCRIÇÃO</td>
            </tr>
        </thead>
        <tbody>
            @foreach($rooms as $room)
            <tr>
            {!! Form::open(['route'=>'schedule.loadSchedule', 'method' => 'post', 'class' => 'form-padrao']) !!}
                <td>{{$room->id}}</td>
                <input type="text" hidden id="id" name="id" value={{$room->id}}>
                <td>{{$room->description}}</td>
                <td>@include ('templates.formulario.submit', ['input' => 'Visualizar'])</td>
            {!! Form::close()!!}
            </tr>
            @endforeach
        </tbody>
    
    </table>
@endsection