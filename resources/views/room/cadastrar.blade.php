@extends('templates.master')
@section('content-view')

@if(session('success'))
<h3>{{ session('success')['messages'] }}</h3>
@endif

{!! Form::open(['route'=>'room.store', 'method' => 'post', 'class' => 'form-padrao']) !!}
    @include ('templates.formulario.input', ['input' => 'description', 'attributes' => ['placeholder'=>'Descrição da sala']])
    @include ('templates.formulario.submit', ['input' => 'Cadastrar'])
{!! Form::close() !!}

@endsection