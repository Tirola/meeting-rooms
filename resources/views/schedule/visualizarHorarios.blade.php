@extends('templates.master')

@section('content-view')
<table class="default-table">
    Horários Locados
        <thead>
            <tr>
               
                <td>DATA</td>
                <td>HORA</td>
                <td>STATUS</td>
            </tr>
        </thead>
        <tbody>
            @foreach($schedules as $schedule)
            <tr>
            {!! Form::open(['route'=>'schedule.loadSchedule', 'method' => 'post', 'class' => 'form-padrao']) !!}
                
                <input type="text" hidden id="id" value={{$schedule->id}}>
                <td>{{$schedule->day}}</td>
                <td>{{$schedule->hour}}H</td>
                <td>{{$schedule->status}}</td>
            {!! Form::close()!!}
            </tr>
            @endforeach
        </tbody>
    
    </table>
    <h3>Reservar sala:</h3>
    @if(session('success'))
        <h3>{{ session('success')['messages'] }}</h3>
    @endif

    {!! Form::open(['route'=>'schedule.store', 'method' => 'post', 'class' => 'form-padrao']) !!}
    
    @include ('templates.formulario.date', ['input' => 'day', 'attributes' => ['placeholder'=>'Data']])    
    @include ('templates.formulario.number', ['input' => 'hour', 'attributes' => ['placeholder'=>'Horário']])
    <input type="text" hidden id="id_room" name="id_room" value={{$id_room}}>
    <input type="text" hidden id="id_user" name="id_user" value={{$id_user}}>
    @include ('templates.formulario.submit', ['input' => 'Reservar'])
    {!! Form::close() !!}

    {!! Form::open(['route'=>'schedule.delete', 'method' => 'post', 'class' => 'form-padrao']) !!}
    
    <input type="text" hidden id="id_user" name="id_user" value={{$id_user}}>
    @include ('templates.formulario.submit', ['input' => 'Remover minha reserva','attributes' =>['data-toggle'=>'modal', 'data-backdrop'=>'static', 'data-target' => '#myModalHorizontal']])
    
    {!! Form::close() !!}


@endsection