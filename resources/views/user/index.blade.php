@extends('templates.master')
@section('css-view')

@endsection


@section('js-view')

@endsection


@section('content-view')

    @if(session('success'))
        <h3>{{ session('success')['messages'] }}</h3>
    @endif

    {!! Form::open(['route'=>'user.store', 'method' => 'post', 'class' => 'form-padrao']) !!}
        @include ('templates.formulario.input', ['input' => 'name', 'attributes' => ['placeholder'=>'Nome']])
        @include ('templates.formulario.input', ['input' => 'email', 'attributes' => ['placeholder'=>'Email']])
        @include ('templates.formulario.password', ['input' => 'password', 'attributes' => ['placeholder'=>'Senha']])
        @include ('templates.formulario.submit', ['input' => 'Cadastrar'])
    {!! Form::close() !!}


@endsection