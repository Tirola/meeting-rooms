<html>
    <head>
    <meta charset="utf-8">
    <title> Login | Meeting Rooms</title>
    <link rel="stylesheet" href="{{ asset('/css/stylesheet.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Fredoka+One" rel="stylesheet">
    </head>
    <body>
        <section id="content-view" class="login">

            <h1>Meeting Rooms</h1>
            <h3>Sistema para gerenciamento de locação de salas de reunião</h3>

            {!! Form::open(['route' => 'dashboard.auth', 'method' => 'post']) !!}
            <p>Acessar o sistema</p>
            <label>
                {!! Form::text('email', null, ['class' => 'input', 'placeholder' => "Email"]) !!}
            </label>

            <label>
            {!! Form::password('password', ['placeholder' => "Senha"]) !!}
            </label>
            {!! Form::submit('Entrar') !!}

            {!! Form::close() !!}

        </section>


    
    </body>
</html>