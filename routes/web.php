<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', ['uses'=>'Controller@homepage']);
Route::post('/logout', ['as'=>'logout', 'uses'=>'Controller@logout']);
Route::get('/cadastrar', ['uses'=>'Controller@cadastrar']);
Route::get('/logout', ['uses'=>'Controller@logout']);
Route::get('/dashboard', ['as'=>'user.dashboard', 'uses'=>'DashboardController@index']);
Route::get('/logar', ['as'=>'user.login', 'uses'=>'Controller@homepage']);
Route::get('/cadastro', ['as'=>'user.cadastro', 'uses'=>'UsersController@index']);
Route::post('/loadSchedule', ['as'=>'schedule.loadSchedule', 'uses'=>'SchedulesController@loadSchedule']);
Route::post('/logar', ['as'=>'dashboard.auth', 'uses'=>'DashboardController@auth']);    
Route::get('/cadastrar', ['as'=>'room.cadastrar', 'uses'=>'RoomsController@cadastrarSala']);
Route::post('/delete', ['as'=>'schedule.delete', 'uses'=>'SchedulesController@delete']);
Route::resource('schedule', 'SchedulesController');
Route::resource('user', 'UsersController');
Route::resource('room', 'RoomsController');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
