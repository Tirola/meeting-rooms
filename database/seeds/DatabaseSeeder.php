<?php

use Illuminate\Database\Seeder;
use App\Entities\User;
use App\Entities\Room;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Tobias Tirola',
            'email' => 'tobiastirola@email.com', 
            'password' => bcrypt('12345678')
        ]);
        User::create([
            'name' => 'Rafael Szarblewski',
            'email' => 'rafaelszarblewski@email.com', 
            'password' => bcrypt('12345678')
        ]);
        Room::create([
            'description' => 'Sala pequena'
        ]);
        Room::create([
            'description' => 'Sala Media'
        ]);
        Room::create([
            'description' => 'Sala Grande'
        ]);
        // $this->call(UsersTableSeeder::class);
    }
}
