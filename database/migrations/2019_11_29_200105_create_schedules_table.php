<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateSchedulesTable.
 */
class CreateSchedulesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('schedules', function(Blueprint $table) {
			$table->increments('id');
			$table->unsignedBigInteger('id_room');
			$table->foreign('id_room')
                ->references('id')->on('rooms')
				->onDelete('cascade');
			$table->unsignedBigInteger('id_user');
			$table->foreign('id_user')
                ->references('id')->on('users')
                ->onDelete('cascade');
			$table->date('day');
			$table->time('hour');
			$table->string('status')->default('Locado');
			$table->timestamps();
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('schedules');
	}
}
